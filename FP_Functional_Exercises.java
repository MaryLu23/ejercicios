import java.util.List;
public class FP_Functional_Exercises{
	public static void main(String[] args){
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		List<String> courses = List.of("Spring", "Spring Boot", "API", 			"Microservices", "AWS", "PCF", "Azure", "Docker", 			"Kubernetes");
		System.out.println("\nEjercicio 1");
		PrintNumImpares(numbers);
		System.out.println("\nEjercicio 2");
		PrintAllCourses(courses);
		System.out.println("\nEjercicio 3");
		PrintCoursesWithSpring(courses);
		System.out.println("\nEjercicio 4");
		PrintCourses4letters(courses);
		System.out.println("\nEjercicio 5");
		printCubeNumbers(numbers);
		System.out.println("\nEjercicio 6");
		printWordsNumberOfCourses(courses);
	}

	/*Imprimir numeros*/
    private static void printNumber(int number){
        System.out.print(number + ", ");
    }
    /*Imprimir String*/
    private static void printString(String course){
        System.out.print(course + ", ");
    }
	private static boolean isOdd(int number){
        return (number % 2 == 1);
    }
 	
	//ejercicio1
	private static void PrintNumImpares(List<Integer> numbers){
		for(int number:numbers){
 			if(number %2!=0){
				System.out.print(number + ",");
			}
		}System.out.println("");
	}
	//ejercicio 2
	private static void PrintAllCourses(List<String> courses){
		for(int i=0; i<=courses.size()-1; i++){
 			System.out.println(courses.get(i));
			
		}
	}
	/*Ejercicio 3*/
        private static void PrintCoursesWithSpring(List<String> courses){
        courses.stream()
            .filter(course -> 					course.contains("Spring"))
	.forEach(FP_Functional_Exercises::printString);
        System.out.println("");
    }


    /*Ejercicio 4*/
    private static void PrintCourses4letters(List<String> courses){
        courses.stream()
            .filter(course -> course.length() >= 4)                         
            .forEach(FP_Functional_Exercises::printString);
        System.out.println("");
    }


    /*Ejercicio 5*/
    private static void printCubeNumbers(List<Integer> numbers){
        numbers.stream()                        
            .filter(FP_Functional_Exercises::isOdd)
            .map(number -> number * number)   
            .forEach(FP_Functional_Exercises::printNumber);
        System.out.println("");
    }


    /*Ejercicio 6*/
    private static void printWordsNumberOfCourses(List<String> courses){
        courses.stream()
            .map(course -> course.length())                         
            .forEach(FP_Functional_Exercises::printNumber);
        System.out.println("");
    }
}
